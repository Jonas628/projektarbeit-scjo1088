# Projektarbeit_scjo1088

[![coverage](https://gitlab.com/hs-karlsruhe/ws2020/scjo1088/projektarbeit-scjo1088/badges/master/coverage.svg)](https://hs-karlsruhe.gitlab.io/hs-karlsruhe/ws2020/scjo1088/projektarbeit-scjo1088/test/)
[![markdownlint](https://hs-karlsruhe.gitlab.io/ws2020/scjo1088/projektarbeit-scjo1088/badges/markdownlint.svg)](https://gitlab.com/hs-karlsruhe/ws2020/scjo1088/projektarbeit-scjo1088/commits/master)
[![yamllint](https://hs-karlsruhe.gitlab.io/ws2020/scjo1088/projektarbeit-scjo1088/badges/yamllint.svg)](https://gitlab.com/hs-karlsruhe/ws2020/scjo1088/projektarbeit-scjo1088/commits/master)
[![pylint](https://hs-karlsruhe.gitlab.io/ws2020/scjo1088/projektarbeit-scjo1088/badges/pylint.svg)](https://hs-karlsruhe.gitlab.io/ws2020/scjo1088/projektarbeit-scjo1088/lint/)
